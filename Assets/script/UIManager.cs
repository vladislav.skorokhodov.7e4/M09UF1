using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public TextMeshProUGUI currentMoneyText;

    private int currentMoney = 0;

    private void Start()
    {
        // Assuming you have assigned the TextMeshProUGUI component in the Inspector
        currentMoneyText.text = currentMoney.ToString();
    }

    private void UpdateMoney(int amount)
    {
        currentMoney += amount;
        currentMoneyText.text = currentMoney.ToString();
    }
}

