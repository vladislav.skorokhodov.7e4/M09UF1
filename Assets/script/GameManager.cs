using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    
    public static int currency; //current money
    public static int clickPower; // how much you get for click
    public static int currencyPerSecond;


    private void Start()
    {
        currency = 0;
        clickPower = 1;
        currencyPerSecond = 0;

        StartCoroutine(GrantCurrency());
    }


    private System.Collections.IEnumerator GrantCurrency()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);  // Wait for 1 second

            currency += currencyPerSecond;  // Grant the currency

            print(currencyPerSecond);
        }
    }
}
