using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeButton : MonoBehaviour
{
    public TextMeshProUGUI currentPriceText;
    public TextMeshProUGUI totalUpgrades;

    private Button self;

    public int initialUpgradePrice;
    public bool autoClick;
    public int upgradePower;
    public float priceMultiplier;

    private int upgradeLevel = 0;

    private int roundedCurrentPrice;


    public void PowerBuy()
    {
        float currentPrice = initialUpgradePrice * Mathf.Pow(priceMultiplier, upgradeLevel);
        roundedCurrentPrice = Mathf.RoundToInt(currentPrice);
        print(currentPrice);


        if (GameManager.currency >= roundedCurrentPrice)
        {
            GameManager.currency -= roundedCurrentPrice;
            upgradeLevel++;

            if (autoClick == true)
            {
                GameManager.currencyPerSecond += upgradePower;
            }
            else
            {
                GameManager.clickPower += upgradePower;
            }
        }
        
    }

    private void Start()
    {
        self = GetComponent<Button>();
    }


    private void Update()
    {
        //for a visual update
        float currentPrice = initialUpgradePrice * Mathf.Pow(priceMultiplier, upgradeLevel);
        roundedCurrentPrice = Mathf.RoundToInt(currentPrice);


        //
        currentPriceText.text = roundedCurrentPrice.ToString();
        totalUpgrades.text = "+"+upgradePower +  " LVL " + upgradeLevel;


        if(GameManager.currency <= roundedCurrentPrice)
        {
            self.interactable = false;
        }
        else
        {
            self.interactable = true;
        }
    }
}
